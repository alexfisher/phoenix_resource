<?php
/**
 * @file
 * phoenix_resource.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function phoenix_resource_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-resource-case_studies'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'resource-case_studies',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'page/case-studies',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-resource-literature'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'resource-literature',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'page/product-literature',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-resource-video_search'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'resource-video_search',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'page/videos',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
