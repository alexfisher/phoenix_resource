<?php
/**
 * @file
 * phoenix_resource.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function phoenix_resource_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in resource_type'.
  $permissions['delete terms in resource_type'] = array(
    'name' => 'delete terms in resource_type',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'eck add compro_component resource entities'.
  $permissions['eck add compro_component resource entities'] = array(
    'name' => 'eck add compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer compro_component resource entities'.
  $permissions['eck administer compro_component resource entities'] = array(
    'name' => 'eck administer compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete compro_component resource entities'.
  $permissions['eck delete compro_component resource entities'] = array(
    'name' => 'eck delete compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit compro_component resource entities'.
  $permissions['eck edit compro_component resource entities'] = array(
    'name' => 'eck edit compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list compro_component resource entities'.
  $permissions['eck list compro_component resource entities'] = array(
    'name' => 'eck list compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view compro_component resource entities'.
  $permissions['eck view compro_component resource entities'] = array(
    'name' => 'eck view compro_component resource entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'edit terms in resource_type'.
  $permissions['edit terms in resource_type'] = array(
    'name' => 'edit terms in resource_type',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
