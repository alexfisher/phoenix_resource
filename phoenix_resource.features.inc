<?php
/**
 * @file
 * phoenix_resource.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phoenix_resource_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function phoenix_resource_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function phoenix_resource_eck_bundle_info() {
  $items = array(
    'compro_component_resource' => array(
      'machine_name' => 'compro_component_resource',
      'entity_type' => 'compro_component',
      'name' => 'resource',
      'label' => 'Resource',
      'config' => array(),
    ),
  );
  return $items;
}
