
/* JavaScript for phoenix resource video */

(function ($) {
  Drupal.behaviors.phoenixResourceVideo = {
    attach: function (context, settings) {
      $video_trigger = $('.video-modal');

      $video_trigger.on('click', function(e) {
        e.preventDefault();

        var $modal = $(this).siblings('.field-name-field-resource-file').find('.modal');

        // Trigger modal.
        $modal.addClass('modal-open');

        // Handle closing of the modal.
        $('.modal-fade-screen, .modal-close').on('click', function() {
          // Stop playing video.
          var $video = $(this).parents('.file').find('.media-youtube-player');
          $video[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');

          // Remove modal class.
          $modal.removeClass('modal-open');
        });

        // Handle click of modal inner.
        $('.modal-inner').on('click', function(e) {
          e.stopPropagation();
        });
      });
    }
  };

})(jQuery);
